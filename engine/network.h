#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QJsonObject>
#include <QString>
#include <QDebug>

class Network : public QObject
{
    Q_OBJECT

public:
    Network();

    QString Connect();

    QString AddressIPv4();

    void WriteNetworkSettings(QJsonObject &JsonObject);

    QJsonObject ShowNetworkSettings();

public slots:
    void setConnect(int index);

    void setAddressIPv4(const QString &_AddreesIPv4);

private:
    QString nConnect;
    QString nIPv4;

    QJsonObject nJsonObject;
};

#endif // NETWORK_H
