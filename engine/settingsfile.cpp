#include "settingsfile.h"
#include <QDebug>

SettingsFile::SettingsFile()
{

}

void SettingsFile::setFileSettingsToSave(QJsonObject &_JsonObject)
{
    JsonObject = _JsonObject;
    qDebug()<< "adres w settings file" << &JsonObject;
}

QJsonObject SettingsFile::FileSettings()
{
    return JsonObject;
}

void SettingsFile::SaveSettings()
{
    QJsonDocument JsonDocument(FileSettings());

    SaveSettingsToFile.setFileName(QString("settings.json"));

    if(!SaveSettingsToFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        Message.critical(nullptr, QString(tr("Zapis ustawień")), QString(tr("Nie można utworzyć bufora zapisu pliku")), QMessageBox::Ok);
    }
    else{
        SaveSettingsToFile.write(JsonDocument.toJson());
        SaveSettingsToFile.flush();
        SaveSettingsToFile.close();

        Message.information(nullptr ,QString(tr("Zapis ustawień")), QString(tr("Plik z ustawieniami został zapisany")), QMessageBox::Ok);
    }
}
