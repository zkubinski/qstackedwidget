#include "network.h"

Network::Network() : nConnect("localhost"), nIPv4("127.0.0.1")
{

}

QString Network::Connect(){
    return nConnect;
}

QString Network::AddressIPv4(){
    return nIPv4;
}

void Network::WriteNetworkSettings(QJsonObject &JsonObject){
    JsonObject["connect"] = Connect();
    JsonObject["ipaddr"] = AddressIPv4();

    qDebug()<< nIPv4;

    nJsonObject = JsonObject;
}

QJsonObject Network::ShowNetworkSettings(){
    return nJsonObject;
}

void Network::setConnect(int index){

    if(index == 0){
        nConnect = "localhost";
    }
    else if(index == 1){
        nConnect = "remotehost";
    }

    qDebug()<< nConnect;
}

void Network::setAddressIPv4(const QString &_AddreesIPv4){
    nIPv4 = _AddreesIPv4;

    nJsonObject.insert("ipaddr", nIPv4);

    qDebug()<< "slot set IP" << nJsonObject.value("ipaddr").toString();
    qDebug()<< "slot set IP w klasie network"<< nIPv4;
}
