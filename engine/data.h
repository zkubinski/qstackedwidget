#ifndef DATA_H
#define DATA_H

#include <QObject>
#include <QJsonObject>
#include <QString>
#include <QDebug>

class Data : public QObject
{
    Q_OBJECT

public:
    Data();

    QString DataDept(){
        return dDataDept;
    }

    void WriteDataSettings(QJsonObject &JsonObject){
        JsonObject["dept"] = dDataDept;

        dJsonObject = JsonObject;
    }

    QJsonObject ShowDataSettings(){
        return dJsonObject;
    }

public slots:
    void setDataDept(int index){

        if(index == 0){
            qDebug()<< "1 Cywilny";
        }
        else if(index == 1){
            qDebug()<< "2 Cywilny";
        }
    }

private:
    QString dDataDept;
    QJsonObject dJsonObject;
};

#endif // DATA_H
