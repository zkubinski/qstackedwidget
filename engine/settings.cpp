#include "settings.h"
#include <QDebug>

Settings::Settings()
{

}

void Settings::CreateSettings(QJsonObject &JsonObject){
    QJsonObject myJsonObjectSettings, JsonObjectNetwork, _JsonObjectNetwork, JsonObjectData, _JsonObjectData;

    myNetworkSettings.WriteNetworkSettings(JsonObjectNetwork);
    _JsonObjectNetwork["network"] = myNetworkSettings.ShowNetworkSettings();

    for(QString &str : _JsonObjectNetwork.keys()){
        myJsonObjectSettings.insert(str, JsonObjectNetwork);
    }

    myDataSettings.WriteDataSettings(JsonObjectData);
    _JsonObjectData["data-from"] = myDataSettings.ShowDataSettings();

    for(QString &str : _JsonObjectData.keys()){
        myJsonObjectSettings.insert(str, JsonObjectData);
    }

    JsonObject["settings"] =  myJsonObjectSettings;
    myJsonObject = JsonObject;
}

QJsonObject &Settings::ShowSettings(){
    return myJsonObject;
}
