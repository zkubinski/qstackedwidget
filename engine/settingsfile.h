#ifndef SETTINGSFILE_H
#define SETTINGSFILE_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QMessageBox>

#include "settings.h"

class SettingsFile : public QObject
{
    Q_OBJECT

public:
    SettingsFile();

    void setFileSettingsToSave(QJsonObject &);
    QJsonObject FileSettings();
    void SaveSettings();
    void ReadSettings();

private:
    Settings mySettings;
    QJsonObject JsonObject;
    QFile SaveSettingsToFile;
    QMessageBox Message;
};

#endif // SETTINGSFILE_H
