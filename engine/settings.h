#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QDebug>

#include "engine/network.h"
#include "engine/data.h"

class Settings : public QObject
{
    Q_OBJECT

public:
    Settings();

public:
    void CreateSettings(QJsonObject &JsonObject);

    QJsonObject &ShowSettings();

public slots:
    void setIPAddressSettings(QString ip){
        myNetworkSettings.setAddressIPv4(ip);
    }

private:
    Network myNetworkSettings;
    Data myDataSettings;
    QJsonObject myJsonObject;
};

#endif // SETTINGS_H
