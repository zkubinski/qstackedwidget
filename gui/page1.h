#ifndef PAGE1_H
#define PAGE1_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QSpacerItem>
#include <QHostAddress>
#include <QLineEdit>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QValidator>

#include <QDebug>

#include "engine/network.h"

class Page1 : public QWidget
{
    Q_OBJECT

public:
    Page1();

private:
    QVBoxLayout *MainLayout;
    QHBoxLayout *Layout, *Layout2;
    QLabel *Label, *Label2, *Label3;
    QComboBox *ComboBox;
    QSpacerItem *Spacer;
    QHostAddress *IPv4Database;
    QLineEdit *IPDatabase;
    QString IPv4;

    QRegularExpression RegexIP;
    QRegularExpressionMatch MatchIP;
    QValidator *ValidateIP;

    Network *mwNetwork;
};

#endif // PAGE1_H
